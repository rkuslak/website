PUBLIC_URL ?= http://localhost:3000/
UID ?= $(shell id -u)
GUID ?= $(shell id -g)
PODMAN ?= $(shell which podman > /dev/null && which podman || which docker)
COMPOSE ?= $(shell which podman > /dev/null && which podman-compose || which docker-compose)
DB_BACKUP_FILE ?= docker/db_backup.sql
DB_CONTAINER ?= website_postgres_1

phony: start logs


start: stop
	${COMPOSE} -p website -f docker/docker-compose.yml build
	${COMPOSE} -p website -f docker/docker-compose.yml up -d

logs:
	${COMPOSE} -p website -f docker/docker-compose.yml logs -f

clean:
	rm -rf public_html/*

make_data:
	mkdir -p --context container_file_t pgadmin_data
	chmod 777 pgadmin_data
	mkdir -p --context container_file_t postgres_data
	chmod 777 postgres_data

build: clean
	mkdir public_html || true
	cd sites/index     && make build install clean PUBLIC_URL=${PUBLIC_URL}
	cd sites/groupcalc && make build install clean PUBLIC_URL=${PUBLIC_URL}/groupcalc
	cp -rv sites/static public_html/
	cp -rv services/resume_api/neoapi/static/* public_html/static

stop:
	${COMPOSE} -p website -f docker/docker-compose.yml down
	
	# NOOP if not using podman
	which podman > /dev/null && ${PODMAN} pod rm --force website || true


db_backup:
	docker-compose -p website -f docker/docker-compose.yml exec postgres \
		pg_dump -U admin neoapi_common | tee ${DB_BACKUP_FILE}

db_restore:
	docker cp ${DB_BACKUP_FILE} ${DB_CONTAINER}:/db_backup.sql
	${COMPOSE} -p website -f docker/docker-compose.yml exec postgres \
			psql -U admin postgres -c 'DROP DATABASE IF EXISTS neoapi_common;'
	${COMPOSE} -p website -f docker/docker-compose.yml exec postgres \
			psql -U admin postgres -c ' CREATE DATABASE neoapi_common TEMPLATE template0;'
	#docker exec ${DB_CONTAINER} psql -U admin postgres -c 'CREATE DATABASE neoapi_common TEMPLATE template0'
	${COMPOSE} -p website -f docker/docker-compose.yml exec postgres \
		bash -c "psql -U admin neoapi_common < /db_backup.sql"
	docker exec ${DB_CONTAINER} rm /db_backup.sql

